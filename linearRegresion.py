import pandas as pd
import numpy as np
import math as m
import datetime
from time import sleep

print("""
///////////////////////////////////////////////
Linear Regression Implementation

Tecnologias de Sistemas Inteligentes 
Professor: PhD. Benjamin Valdes Aguirre 

Moises Diaz Malagon A01208580

February 12, 2018 
///////////////////////////////////////////////""")

#Auxiliar function to get the linear distance between coordinates
#Auxiliar function to get the linear distance between coordinates
def distance(lat1, lng1, lat2, lng2):
    #return distance as meter if you want km distance, remove "* 1000"
    radius = 6371 

    dLat = (lat2-lat1) * m.pi / 180
    dLng = (lng2-lng1) * m.pi / 180

    lat1 = lat1 * m.pi / 180
    lat2 = lat2 * m.pi / 180

    val = m.sin(dLat/2) * m.sin(dLat/2) + m.sin(dLng/2) * m.sin(dLng/2) * m.cos(lat1) * m.cos(lat2)    
    ang = 2 * m.atan2(np.sqrt(val), m.sqrt(1-val))
    return radius * ang

def day_of_week(strDate):
    year, month, day = (int(x) for x in strDate.split('-'))
    return datetime.date(year, month, day).weekday()

def preprocessing_csv():
    
    print("Reading CSV")
    df = pd.read_csv('train.csv')[0:10000]
    print("Creating Pickup hour information")
    df['pick_up_hour'] = df.apply(lambda row: int(row.pickup_datetime[-8:-6]), axis=1)
    print("Creating Day Of Week information")
    df['day_of_week'] = df.apply(lambda row: day_of_week(row.pickup_datetime[0:10]), axis=1)
    print("Creating Month information")
    df['month'] = df.apply(lambda row: int(row.pickup_datetime[8:10]), axis=1)
    print("Creating Distance To Travel information")
    df['distance_to_travel'] = df.apply(lambda row: distance(row.pickup_latitude, row.pickup_longitude, row.dropoff_latitude, row.dropoff_longitude), axis=1)
    df.to_pickle("preprocessed_data") 
    return df

#Preprocess samples
try: 
    df = pd.read_pickle("preprocessed_data")
    print("Preprocessed Data Found")
except:
    print("Preprocessed file doesn't exist. Creating file.")
    df = preprocessing_csv()

    
print("Getting the samples")
# x_samples = df[['pick_up_hour', 'passenger_count', 'pickup_longitude', 'pickup_latitude', 'dropoff_longitude', 'dropoff_latitude']].as_matrix()
x_samples = df[['vendor_id', 'passenger_count', 'month','pick_up_hour', 'distance_to_travel', 'day_of_week', 'pickup_latitude', 'pickup_longitude', 'dropoff_latitude', 'dropoff_longitude']].as_matrix()
y_output_array = df['trip_duration'].as_matrix()
# Test in an school
# Pre test and post test of a course
# To find an estimate of post from the pre grade

ALPHA = .015
scaling_factors = []
scaled_samples = []
channel_num = 0

# x_samples = np.array([
#     [1,2],
#     [2,4],
#     [3,9],
#     [4,16],
#     [5,25]
# ])

# y_output_array = np.array([0,10,20,30,40])


theta_array = np.random.rand(len(x_samples[0])+1)


def normalize_input(x_samples):
    global channel_num
    channel_num = len(x_samples[0])
    
    for i in range(channel_num):
        channel_mean = np.mean([x[i] for x in x_samples])
        channel_std_dev = np.std([x[i] for x in x_samples])
        print("Channel {} > mean: {} std: {}".format(i, channel_mean, channel_std_dev))
        scaling_factors.append({'mean': channel_mean, 'std': channel_std_dev})

    for sample in x_samples:
        scaled_sample = []
        for channel in range(channel_num):
            new_value = (sample[channel] - scaling_factors[channel]['mean']) / scaling_factors[channel]['std']
            scaled_sample.append(new_value)
        scaled_samples.append(scaled_sample)

    return scaled_samples

def normalize_single_input(sample):
    scaled_sample = []
    for channel in range(channel_num):
        new_value = (sample[channel] - scaling_factors[channel]['mean']) / scaling_factors[channel]['std']
        scaled_sample.append(new_value)
    return scaled_sample


def hyp(x_sample):
    ret_val = theta_array[0] #bias plus first theta by first input
    for i in range(len(x_sample)):
        ret_val += theta_array[i + 1]*x_sample[i]
    return ret_val


def cost_function(normalized_samples):
    summation = 0
    for i in range(len(normalized_samples)):
        summation += (hyp(normalized_samples[i]) - y_output_array[i])**2
    return summation/(2*len(normalized_samples))

def optimize(normalized_samples):
    for j in range(len(theta_array)):
        summation = 0
        for i in range(len(normalized_samples)):
            if j == 0:
                multiplier = 1
            else:
                multiplier = normalized_samples[i][j - 1]

            summation += (hyp(normalized_samples[i]) - y_output_array[i])*multiplier

        theta_array[j] = theta_array[j] - ALPHA*summation/len(normalized_samples) #theta update


def print_current_status(normalized_samples):
    for i in range(len(normalized_samples)):
        print("\nyhyp: {}   y: {}".format(hyp(normalized_samples[i]), y_output_array[i]))
    print("-------------------------------------------")


def print_thetas_status():
    for i in range(len(theta_array)):
        print("Theta({}): {}".format(i, theta_array[i]))
    print("-------------------------------------------")

if __name__ == "__main__":

    print("Normalizing samples")
    normalized_samples = normalize_input(x_samples)
    np.save("normalization_parameters", scaling_factors)
    #print_current_status(normalized_samples)
    lastError = 0
    currentError = np.abs(cost_function(normalized_samples))
    print("Running linear regression training")
    while (( currentError > 0.1) and (abs(lastError - currentError) > 0.00001)):
        # print_current_status(normalized_samples)
        # print_thetas_status()
        np.save("outfile_training", theta_array)
        optimize(normalized_samples)
        lastError = currentError
        currentError = np.abs(cost_function(normalized_samples))
        print ("\r Cost Value: {0:.2f} ".format(currentError), end="")

    print("Finished")
    # print(x_samples)
    # print(normalized_samples)
    # print(normalize_single_input([17, 1.49852078, 0, 40.767937, -73.982155, 40.765602]))
    print_current_status(normalized_samples)
    # print(hyp(normalize_single_input([17, 1.49852078, 0, 40.767937, -73.982155, 40.765602])))
    np.save("outfile_training_final", theta_array)
