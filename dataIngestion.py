import pandas as pd
import numpy as np

df = pd.read_csv('train.csv')

# df[[0:3]]

# Returns the hour of the day
df['pickup_datetime'][0:3][-8:-6]




hour_of_day = [df['pickup_datetime'][i][-8:-6] for i in range(df.size)]

df['pick_up_hour'] = df.apply(lambda row: row.pickup_datetime[-8:-6], axis=1)
 
# Passenger count
df['passenger_count'][0]

# Linear distance
np.sqrt((df['pickup_longitude'][0] - df['dropoff_longitude'][0])**2 + (df['pickup_latitude'][0] - df['dropoff_latitude'][0])**2)


#To get the samples
df['pick_up_hour'] = df.apply(lambda row: int(row.pickup_datetime[-8:-6]), axis=1)
df[['pick_up_hour', 'passenger_count', 'pickup_longitude', 'pickup_latitude', 'dropoff_longitude', 'dropoff_latitude']].as_matrix()
df['trip_duration'].as_matrix()