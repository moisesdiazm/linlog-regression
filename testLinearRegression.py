import pandas as pd
import numpy as np
import math as m
import datetime
from time import sleep

print("""
///////////////////////////////////////////////
Linear Regression TESTING

Tecnologias de Sistemas Inteligentes 
Professor: PhD. Benjamin Valdes Aguirre 

Moises Diaz Malagon A01208580

February 12, 2018 
///////////////////////////////////////////////""")

#Auxiliar function to get the linear distance between coordinates
#Auxiliar function to get the linear distance between coordinates
def distance(lat1, lng1, lat2, lng2):
    #return distance as meter if you want km distance, remove "* 1000"
    radius = 6371 

    dLat = (lat2-lat1) * m.pi / 180
    dLng = (lng2-lng1) * m.pi / 180

    lat1 = lat1 * m.pi / 180
    lat2 = lat2 * m.pi / 180

    val = m.sin(dLat/2) * m.sin(dLat/2) + m.sin(dLng/2) * m.sin(dLng/2) * m.cos(lat1) * m.cos(lat2)    
    ang = 2 * m.atan2(np.sqrt(val), m.sqrt(1-val))
    return radius * ang

def day_of_week(strDate):
    year, month, day = (int(x) for x in strDate.split('-'))
    return datetime.date(year, month, day).weekday()

def preprocessing_csv():
    print("Reading CSV")
    df = pd.read_csv('train.csv')[90:100]
    print("Creating Pickup hour information")
    df['pick_up_hour'] = df.apply(lambda row: int(row.pickup_datetime[-8:-6]), axis=1)
    print("Creating Day Of Week information")
    df['day_of_week'] = df.apply(lambda row: day_of_week(row.pickup_datetime[0:10]), axis=1)
    print("Creating Month information")
    df['month'] = df.apply(lambda row: int(row.pickup_datetime[8:10]), axis=1)
    print("Creating Distance To Travel information")
    df['distance_to_travel'] = df.apply(lambda row: distance(row.pickup_latitude, row.pickup_longitude, row.dropoff_latitude, row.dropoff_longitude), axis=1)
    df.to_pickle("preprocessed_data") 
    return df

#Preprocess samples
df = preprocessing_csv()
    
print("Getting the samples")


theta_array = np.load("outfile_training_final.npy")
scaling_factors = np.load("normalization_parameters.npy")
channel_num = 0

def normalize_single_input(sample):
    channel_num = len(sample)
    scaled_sample = []
    for channel in range(channel_num):
        new_value = (sample[channel] - scaling_factors[channel]['mean']) / scaling_factors[channel]['std']
        scaled_sample.append(new_value)
    return scaled_sample


def hyp(x_sample):
    ret_val = theta_array[0] #bias plus first theta by first input
    for i in range(len(x_sample)):
        ret_val += theta_array[i + 1]*x_sample[i]
    return ret_val

if __name__ == "__main__":

    print("Normalizing samples")
    print(theta_array)
    #'pick_up_hour', 'distance_to_travel', 'day_of_week', 'pickup_latitude', 'pickup_longitude', 'dropoff_latitude'
    print("Running linear regression on data")
    df['estimated_time'] = df.apply(lambda row: hyp(normalize_single_input([row.vendor_id, row.passenger_count, row.month, row.pick_up_hour, row.distance_to_travel, row.day_of_week, row.pickup_latitude, row.pickup_longitude, row.dropoff_latitude, row.dropoff_longitude])), axis=1)
    print("Finished")
    df.to_csv('out.csv')
