import pandas as pd
import numpy as np
import math as m
from time import sleep

print("Hello World")

#Auxiliar function to get the linear distance between coordinates
def distance(lat1, lng1, lat2, lng2):
    #return distance as meter if you want km distance, remove "* 1000"
    radius = 6371 * 1000 

    dLat = (lat2-lat1) * m.pi / 180
    dLng = (lng2-lng1) * m.pi / 180

    lat1 = lat1 * m.pi / 180
    lat2 = lat2 * m.pi / 180

    val = m.sin(dLat/2) * m.sin(dLat/2) + m.sin(dLng/2) * m.sin(dLng/2) * m.cos(lat1) * m.cos(lat2)    
    ang = 2 * m.atan2(np.sqrt(val), m.sqrt(1-val))
    return radius * ang

#Get the samples
print("Reading CSV")
df = pd.read_csv('train.csv')

print("Creating Pickup hour calculus")
print(df.apply(lambda row: distance(row.pickup_latitude, row.pickup_longitude, row.dropoff_latitude, row.dropoff_longitude), axis=1))